import json
import time
import numpy as np
from django.db.models import Max
from django.shortcuts import render

from tqdm import tqdm
from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser
from django.templatetags.static import static
from rest_framework import status

from Back.settings import BASE_DIR, JSON_LOC
from games import getBanGames
from games.models import Games, InvolvedCompany, Company, Cover
from games.models import ReleaseDate
from games.serializers import GamesSerializer, ReleaseDateSerializer, InvolvedCompanySerializer, CompanySerializer
from games.models import Games, Genre
from games.serializers import GamesSerializer
from rest_framework.decorators import api_view

from django.conf.urls import url

from users.forms import DocumentForm
from users.models import Users, UsersPreference, UsersReco
from users import calculCluster
from users.models import Users, UsersPreference, UsersReco, UsersCluster
from users.serializers import UsersSerializer, UsersPreferenceSerializer

from django.views.decorators.csrf import csrf_exempt


@api_view(['GET', 'POST', 'DELETE'])
def games_list(request):
    if request.method == 'GET':
        game = Games.objects.filter()[:15]
        cover_tab = []
        games_serializer = GamesSerializer(game, many=True)
        for i in range(0, game.count() - 1):
            if int(float(games_serializer.data[i]['cover_2'])) != -1:
                obj_cover = Cover.objects.get(id=int(float(games_serializer.data[i]['cover_2'])))
                cover = {
                    "cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
            else:
                cover = {"cover_url": "no_cover"}
            cover_tab.append(cover)

        # cover = {"cover_slug": "https://images.igdb.com/igdb/image/upload/t_1080p/" + + ".jpg"}

        data = [games_serializer.data,
                cover_tab
                ]

        return JsonResponse(data, safe=False)

    elif request.method == 'POST':
        games_data = JSONParser().parse(request)
        games_serializer = GamesSerializer(data=games_data)
        if games_serializer.is_valid():
            games_serializer.save()
            return JsonResponse(games_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(games_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Games.objects.all().delete()
        return JsonResponse({'message': '{} games were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


def get_game_infos(request, idgame):
    if request.method == 'GET':
        game = Games.objects.get(id=idgame)
        studiodev = InvolvedCompany.objects.filter(game_id=idgame)

        rds_tab = []
        genre_tab = []
        age_rating_tab = []
        multiplayer_tab = []
        platform_tab = []
        website_tab = []
        studiodevname_tab = []
        screenshot_tab = []

        i = 0
        for screenshot in game.screenshots.all():
            screenshots = {
                "id": screenshot.id,
                "screenshot_slug": "https://images.igdb.com/igdb/image/upload/t_1080p/" + screenshot.image_id + ".jpg"

            }
            screenshot_tab.append(screenshots)
            i += 1

        i = 0
        for website in game.websites.all():  # release date
            websites = {
                "id": website.id,
                "url": website.url
            }
            website_tab.append(websites)
            i += 1

        i = 0
        for platform in game.platform.all():  # release date
            platforms = {
                "id": platform.id,
                "name": platform.name
            }
            platform_tab.append(platforms)
            i += 1

        i = 0
        for rd in game.release_date.all():  # release date
            rds = {
                "id": rd.id,
                "date": str(rd.date),
                "region": rd.region.name
            }
            rds_tab.append(rds)
            i += 1

        i = 0
        for multiplayer in game.multiplayer.all():
            multiplayers = {
                "online_coop": multiplayer.online_coop,
                "split_scree": multiplayer.split_screen
            }
            multiplayer_tab.append(multiplayers)
            i += 1

        i = 0
        for genre in game.genre.all():  # genre

            genres = {
                "id": genre.id,
                "name": genre.name
            }
            genre_tab.append(genres)
            i += 1

        '''
        i = 0
        for AgeRating in game.age_rating.all(): #AgeRating
            ages_ratings = {
                "id": AgeRating.id,
                "rating": AgeRating.rating,
                "category": AgeRating.category,
                "synopsis": AgeRating.synopsis
            }
            age_rating_tab.append(ages_ratings)
            i += 1
        '''

        serializer_studiodev = InvolvedCompanySerializer(studiodev, many=True)
        if studiodev.count() != 0:
            i = 0
            for i in (0, studiodev.count() - 1):
                obj_studiodev_name = Company.objects.get(id=serializer_studiodev.data[i]['company'])
                studiodev_name = {"name": obj_studiodev_name.name}  # status
                studiodevname_tab.append(studiodev_name)

        category = {"category_name": game.category.name}  # category
        status = {"status": game.status.name}  # status
        if game.cover_2.id != -1:
            cover = {
                "cover_slug": "https://images.igdb.com/igdb/image/upload/t_1080p/" + game.cover_2.image_id + ".jpg"}  # cover
        else:
            cover = {"cover_slug": "no_cover"}

        serializer_game = GamesSerializer(game)

        data = [serializer_game.data,
                genre_tab,
                age_rating_tab,
                rds_tab,
                status,
                multiplayer_tab,
                category,
                platform_tab,
                website_tab,
                studiodevname_tab,
                cover,
                screenshot_tab
                ]
        return JsonResponse(data, safe=False)


def get_best_rated_games(request):
    games = []
    ids = getbestgames(1, 3, 15)
    for id in ids:

        game = Games.objects.get(id=id[0])
        ser = GamesSerializer(game)
        rating = {"reco-rating": id[1]}

        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}

        games.append((ser.data, rating, cover))
    return JsonResponse(games, safe=False)


def get_best_new_games(request):
    games = []
    ids = getbestgames(3, 1, 15)
    for id in ids:
        game = Games.objects.get(id=id[0])
        ser = GamesSerializer(game)
        rating = {"reco-rating": id[1]}
        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}

        games.append((ser.data, rating, cover))
    return JsonResponse(games, safe=False)


def get_best_rated_games(request, idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    gamesID = userReco.gameListBest.split(",")
    gamesID = [int(numeric_string) for numeric_string in gamesID]
    games = []
    for id in gamesID:
        game = Games.objects.get(id=id)
        ser = GamesSerializer(game)
        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}
        games.append((ser.data, 0, cover))
    return JsonResponse(games, safe=False)


def get_best_preference_games(request, idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    gamesID = userReco.gameListForYou.split(",")
    gamesID = [int(numeric_string) for numeric_string in gamesID]
    games = []
    for id in gamesID:
        game = Games.objects.get(id=id)
        ser = GamesSerializer(game)
        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}
        games.append((ser.data, 0, cover))
    return JsonResponse(games, safe=False)


def getLikedGame(request, idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    gamesID = userReco.gameListLike.split(",")
    gamesID = [int(numeric_string) for numeric_string in gamesID]
    games = []
    for id in gamesID:
        game = Games.objects.get(id=id)
        ser = GamesSerializer(game)
        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}
        games.append((ser.data, 0, cover))
    return JsonResponse(games, safe=False)


def getListGame(request, idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    gamesID = userReco.gameListMyList.split(",")
    gamesID = [int(numeric_string) for numeric_string in gamesID]
    gamesID.reverse()
    games = []
    for id in gamesID:
        game = Games.objects.get(id=id)
        ser = GamesSerializer(game)
        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}
        games.append((ser.data, 0, cover))
    return JsonResponse(games, safe=False)


def get_best_new_games(request, idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    gamesID = userReco.gameListFresh.split(",")
    gamesID = [int(numeric_string) for numeric_string in gamesID]
    games = []
    for id in gamesID:
        game = Games.objects.get(id=id)
        ser = GamesSerializer(game)
        if int(float(ser.data['cover_2'])) != -1:
            obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
            cover = {"cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
        else:
            cover = {"cover_url": "no_cover"}
        games.append((ser.data, 0, cover))
    return JsonResponse(games, safe=False)


def get_other_liked_games(request, idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    if userReco.gameListOthersLike == "null":
        return JsonResponse({'statut': -1})
    else:
        gamesID = userReco.gameListOthersLike.split(",")
        gamesID = [int(numeric_string) for numeric_string in gamesID]
        gamesID = list(dict.fromkeys(gamesID))
        games = []
        for id in gamesID:
            game = Games.objects.get(id=id)
            ser = GamesSerializer(game)
            if int(float(ser.data['cover_2'])) != -1:
                obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
                cover = {
                    "cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
            else:
                cover = {"cover_url": "no_cover"}
            games.append((ser.data, 0, cover))
        return JsonResponse(games, safe=False)


def update_recos(idUser):
    pondFraicheur = [1, 12, 2]
    pondRating = [1, 4, 5]
    pondPrefUtilisateur = [3, 1, 1]
    numberOfGames = 20
    idPref = []
    for i in range(3):
        if i == 1:
            numberOfGames += 20
        ids = getbestgames(pondFraicheur[i], pondRating[i], pondPrefUtilisateur[i], idUser, numberOfGames)
        gamesID = []
        for id in ids:
            gamesID.append(id[0])
        string = ''
        if i > 0:
            gamesID = removeDuplicate(idPref, gamesID)
        for gameid in gamesID[:20]:
            string += str(gameid) + ","
        string = string[:-1]
        userReco = UsersReco.objects.get(user_id=idUser)
        if i == 0:
            idPref = gamesID
            userReco.gameListForYou = string
        if i == 1:
            userReco.gameListFresh = string
        if i == 2:
            userReco.gameListBest = string
        userReco.save()
    return JsonResponse("Update ended successfully!", safe=False)


def removeDuplicate(liste1, liste2):
    l3 = [x for x in liste2 if x not in liste1]
    return l3


def calculScoreGameLatest(idGames, idUser):
    fresh = 0
    notes = []
    note = 0
    prefuser = 0
    for idGame in idGames:
        game = Games.objects.get(id=idGame)
        note = int(game.aggregated_rating)
        notes.append(note)
    print(notes)


import datetime


def getUnbanGames():
    file = open(str(JSON_LOC) + '/' + 'unbanGames.json')
    return json.load(file)


def getbestgames(pondFraicheur, pondRating, pondPrefUtilisateur, idUser, number):
    unbanGames = getUnbanGames()
    games = Games.objects.filter(id__in=unbanGames).order_by('id')
    maxVote = Games.objects.aggregate(Max('rating_count'))

    listGenre = []
    user = Users.objects.get(id=idUser)
    userPrefs = UsersPreference.objects.filter(user=user)
    for userPref in userPrefs:
        listGenre.append((userPref.genre.id, userPref.value))
    listGenre = sorted(listGenre, key=lambda value: value[1])
    listGenre.reverse()
    listGenre = listGenre[:3]

    list = []
    for game in tqdm(games):
        scoreFraicheur = round(calculScoreFraicheur(game))
        scoreRating = round(calculScoreRating(game, maxVote['rating_count__max']))
        scorePrefUtilisateur = round(calculScorePrefUtilisateur(game, listGenre))
        moy = (
                          pondFraicheur * scoreFraicheur + pondRating * scoreRating + pondPrefUtilisateur * scorePrefUtilisateur) / (
                          pondFraicheur + pondRating + pondPrefUtilisateur)
        list.append((game.id, moy))
    list = sorted(list, key=lambda moy: moy[1])
    list.reverse()
    finalist = list[:number]
    return finalist


def calculScoreRating(game, maxVote):
    nbVote = game.rating_count
    score_nbVote = 100 - ((maxVote - nbVote) * 0.02)
    score_final = (game.rating + score_nbVote) / 2
    return score_final


def calculScoreFraicheur(game):
    mid_date = datetime.date(2014, 1, 1)
    score = 50
    game_date = datetime.date(5000, 1, 1)
    for rd in game.release_date.all():
        if rd.date < game_date:
            game_date = rd.date
    num_months = (game_date.year - mid_date.year) * 12 + (game_date.month - mid_date.month)
    if num_months < 0:
        score += num_months * 0.025
    else:
        score += num_months * 0.52
    if score > 100:
        score = 100
    if score < 0:
        score = 0
    return score


def calculScorePrefUtilisateur(game, listGenre):
    scoreJeu = 50

    genreCommun = []
    for genre in game.genre.all():
        for (x, y) in listGenre:
            if genre.id == x:
                genreCommun.append((genre.id, y))
    nbGenreCommun = len(genreCommun)
    if nbGenreCommun == 3:
        scoreJeu += 45
    elif nbGenreCommun == 2:
        if game.genre.count == 2:
            scoreJeu += 35
        else:
            scoreJeu += 25
    elif nbGenreCommun == 1:
        if game.genre.count == 1:
            scoreJeu += 25
        else:
            scoreJeu += 10
    else:
        scoreJeu += 0
    return scoreJeu


def recalcul_cluster_games():
    users = Users.objects.all().order_by('id')
    userMeta = []
    dataset = []
    for user in users:
        userDatas = UsersPreference.objects.filter(user=user).order_by('genre')
        for userData in userDatas:
            userMeta.append(userData.value)
        dataset.append(userMeta)
        userMeta = []
    nbUsers = Users.objects.count()
    clusters = calculCluster(dataset, int(nbUsers / 5))
    usersClusters = UsersCluster.objects.all()
    usersClusters.delete()
    for i in range(nbUsers):
        userCluster = UsersCluster(user=users[i], idCluster=clusters[i])
        userCluster.save()
        print(str(users[i].username) + " || Cluster n°" + str(clusters[i]))


def userLike(request, idUser, idGame):
    game = Games.objects.get(id=idGame)
    user = Users.objects.get(id=idUser)
    catIds = []
    for cat in game.genre.all():
        catIds.append(cat.id)
    userPrefs = UsersPreference.objects.filter(user=user)
    for userPref in userPrefs:
        if userPref.genre.id in catIds:
            userPref.value += 5
        else:
            userPref.value -= 5
        if userPref.value > 100:
            userPref.value = 100
        if userPref.value < 40:
            userPref.value = 40
        userPref.save()
    userReco = UsersReco.objects.get(user=user)
    userReco.interactionsNb += 1
    userReco = addOrRemoveLikeGame(userReco, idGame)
    userReco.save()
    if userReco.interactionsNb % 5 == 0:
        print("======== Recalcul des recommandations... ========")
        update_recos(idUser)
        print("======== Recalcul des clusters... ========")
        recalcul_cluster_games()
        print("======== Mise à jour de la liste des jeux aimés par les autres utilisateurs... ========")
        users = Users.objects.all()
        for userCluster in users:
            update_otherslike(userCluster.id)
        print("======== Mise à jour réussie avec succès ! ========")
    return JsonResponse("end", safe=False)


def update_otherslike(idUser):
    userReco = UsersReco.objects.get(user_id=idUser)
    user = Users.objects.get(id=idUser)
    userCluster = UsersCluster.objects.get(user=user).idCluster
    others = UsersCluster.objects.filter(idCluster=userCluster)
    otherUsers = []
    for other in others:
        otherUsers.append(other.user)
    string = ''
    gameFound = 0
    for otherUser in otherUsers:
        if int(otherUser.id) != int(idUser):
            otherUserReco = UsersReco.objects.get(user=otherUser)
            content = otherUserReco.gameListLike
            if content != "null":
                gameFound += 1
                gameLiked = otherUserReco.gameListLike.split(",")
                gameLiked = [int(numeric_string) for numeric_string in gameLiked]
                for gameid in gameLiked:
                    string += str(gameid) + ","
    if gameFound == 0:
        string = 'null'
    else:
        string = string[:-1]
    userReco.gameListOthersLike = string
    userReco.save()


def addOrRemoveLikeGame(userReco, idGame):
    content = userReco.gameListLike
    if content != "null":
        gameLiked = userReco.gameListLike.split(",")
        gameLiked = [int(numeric_string) for numeric_string in gameLiked]
        if int(idGame) in gameLiked:
            gameLiked.remove(int(idGame))
        else:
            gameLiked.append(int(idGame))
        string = ''
        if len(gameLiked) < 1:
            string = 'null'
        else:
            for gameid in gameLiked:
                string += str(gameid) + ","
            string = string[:-1]
        userReco.gameListLike = string
    else:
        string = str(idGame)
        userReco.gameListLike = string
    return userReco


def addOrRemoveListGame(request, idUser, idGame):
    userReco = UsersReco.objects.get(user_id=idUser)
    content = userReco.gameListMyList
    if content != "null":
        gameListMyList = userReco.gameListMyList.split(",")
        gameListMyList = [int(numeric_string) for numeric_string in gameListMyList]
        if int(idGame) in gameListMyList:
            gameListMyList.remove(int(idGame))
        else:
            gameListMyList.append(int(idGame))
        string = ''
        if len(gameListMyList) < 1:
            string = 'null'
        else:
            for gameid in gameListMyList:
                string += str(gameid) + ","
            string = string[:-1]
        userReco.gameListMyList = string
    else:
        print("here")
        string = str(idGame)
        userReco.gameListMyList = string
    userReco.save()
    return JsonResponse("end", safe=False)


def writeUnbanGames(request):
    print("Parcours des jeux en cours...")
    start = time.time()
    games = Games.objects.all().order_by('id')
    unbanGames = []
    for game in tqdm(games):
        if game.release_date.count() > 0 and game.platform.count() > 0 and game.genre.count() > 0 and game.category.value == 0 and game.rating != -1 and game.rating_count != -1:
            unbanGames.append(game.id)
    with open(str(JSON_LOC) + '/' + 'unbanGames.json', 'w') as file:
        json.dump(unbanGames, file)
    print("Nombre d'id bannis : " + str(len(unbanGames)))
    print("Temps écoulé : " + str(round(time.time() - start)) + "s")


@csrf_exempt
def research_game(request):
    if request.method == 'POST':
        games_array = []
        data = JSONParser().parse(request)
        limit = 20
        offset = int(data['page']) * 20
        games = Games.objects.filter(name__icontains=data['text'], rating__isnull=False).order_by('-rating')[offset:offset+limit]
        for game in games:
            ser = GamesSerializer(game)
            if int(float(ser.data['cover_2'])) != -1:
                obj_cover = Cover.objects.get(id=int(float(ser.data['cover_2'])))
                cover = {
                    "cover_url": "https://images.igdb.com/igdb/image/upload/t_1080p/" + obj_cover.image_id + ".jpg"}
            else:
                cover = {"cover_url": "no_cover"}
            games_array.append((ser.data, 0, cover))
        print(games_array)
        return JsonResponse(games_array, safe=False)


def getAllCats(request):
    cats = Genre.objects.all().order_by('name')
    result = []
    for cat in cats:
        id = {"id": cat.id}
        name = {"name": cat.name}
        result.append((id, name))
    return JsonResponse(result, safe=False)