from django.db import models

import os
import sys
#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Back.settings')

class Franchise(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True, default='-1')


class Region(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True)


class DateCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=256, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True)


class GameMode(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')


class Genre(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')


class GameVideo(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    video_id = models.CharField(max_length=255, null=False, default='')


class Keyword(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')


class Platform(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')


class GameEngine(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    description = models.CharField(max_length=500, null=True)


class MultiplayerMode(models.Model):
    id = models.IntegerField(primary_key=True)
    platform = models.ForeignKey(Platform, to_field='id', blank=False, default='', on_delete=models.PROTECT)
    campaign_coop = models.BooleanField(null=False, default=False)
    drop_in = models.BooleanField(null=False, default=False)
    lan_coop = models.BooleanField(null=False, default=False)
    offline_coop = models.BooleanField(null=False, default=False)
    offline_coop_max = models.IntegerField(null=True)
    offline_max = models.IntegerField(null=True)
    online_coop = models.BooleanField(null=False, default=False)
    online_coop_max = models.IntegerField(null=True)
    online_max = models.IntegerField(null=True)
    split_screen = models.BooleanField(null=False, default=False)


class AgeRatingCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True)


class AgeRatingRating(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True)

class AgeRating(models.Model):
    id = models.IntegerField(primary_key=True)
    category = models.ForeignKey(AgeRatingCategory, to_field='value', null=False, blank=False, default='', on_delete=models.PROTECT)
    rating = models.ForeignKey(AgeRatingRating, to_field='value', null=False, blank=False, default='', on_delete=models.PROTECT)
    synopsis = models.CharField(max_length=5000, null=True)


class ReleaseDate(models.Model):
    id = models.IntegerField(primary_key=True)
    date = models.DateField(null=False, default='django.utils.timezone.now')
    category = models.ForeignKey(DateCategory, to_field='value', blank=False, default='', on_delete=models.PROTECT)
    date = models.DateField(null=True)
    human = models.CharField(max_length=255, null=False, default='')
    m = models.IntegerField(null=True)
    platform = models.ForeignKey(Platform, to_field='id', blank=False, default='', on_delete=models.PROTECT)
    region = models.ForeignKey(Region, to_field='value', blank=False, default='', on_delete=models.PROTECT)
    y = models.IntegerField(null=True)


class Status(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True)


class Theme(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')


class Cover(models.Model):
    id = models.IntegerField(primary_key=True)
    alpha_channel = models.BooleanField()
    animated = models.BooleanField()
    height = models.IntegerField(max_length=11)
    image_id = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    width = models.IntegerField(max_length=11)


class Screenshot(models.Model):
    id = models.IntegerField(primary_key=True)
    alpha_channel = models.BooleanField()
    animated = models.BooleanField()
    height = models.IntegerField(max_length=11)
    image_id = models.CharField(max_length=255)
    url = models.CharField(max_length=255)
    width = models.IntegerField(max_length=11)


class WebsiteCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    value = models.IntegerField(max_length=11, null=False, unique=True)


class Website(models.Model):
    id = models.IntegerField(primary_key=True)
    trusted = models.BooleanField(null=False, default=False)
    url = models.CharField(max_length=2083, null=False, default='')
    category = models.ForeignKey(WebsiteCategory, to_field='value', blank=False, default='', on_delete=models.PROTECT)


class CompanyWebsite(models.Model):
    id = models.IntegerField(primary_key=True)
    trusted = models.BooleanField(null=False, default=False)
    url = models.CharField(max_length=2083, null=False, default='')
    category = models.ForeignKey(WebsiteCategory, to_field='value', blank=False, default='', on_delete=models.PROTECT)


class Games(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    cover = models.CharField(max_length=2083, null=True)  # Liens d'images
    #first_release_date = models.DateField(null=True)
    slug = models.CharField(max_length=255, null=True)  # Titre sans espace accent tiret ect
    status = models.ForeignKey(Status, to_field='value', blank=False, default='', on_delete=models.PROTECT)
    storyline = models.CharField(max_length=50000, null=True)
    summary = models.CharField(max_length=50000, null=True)
    aggregated_rating = models.FloatField(null=True,)  # moyenne ?
    aggregated_rating_count = models.IntegerField(null=False, default=0)
    multiplayer = models.ManyToManyField(MultiplayerMode, blank=False, default='')
    franchise = models.ManyToManyField(Franchise, blank=False, default='')  # table association dans la bdd normalement
    category = models.ForeignKey(Category, to_field='value', blank=False, default='', on_delete=models.PROTECT)
    age_rating = models.ManyToManyField(AgeRating, blank=False, default='')
    game_engine = models.ManyToManyField(GameEngine, blank=False, default='')
    game_mode = models.ManyToManyField(GameMode, blank=False, default='')
    genre = models.ManyToManyField(Genre, blank=False, default='')
    keyword = models.ManyToManyField(Keyword, blank=False, default='')
    platform = models.ManyToManyField(Platform, blank=False, default='')
    release_date = models.ManyToManyField(ReleaseDate, blank=False, default='')
    video = models.ManyToManyField(GameVideo, blank=False, default='')
    websites = models.ManyToManyField(Website, blank=False, default='')
    screenshots = models.ManyToManyField(Screenshot)
    themes = models.ManyToManyField(Theme, blank=False, default='')
    screenshots = models.ManyToManyField(Screenshot, blank=False, default='')
    cover_2 = models.ForeignKey(Cover, null=True, on_delete=models.PROTECT)
    rating = models.FloatField(null=True)
    rating_count = models.IntegerField(null=True, default=0)


class AlternativeName(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=True)
    game = models.ForeignKey(Games, on_delete=models.CASCADE, null=False, default='')


class Company(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=False, default='')
    description = models.CharField(max_length=50000, null=True)
    websites = models.ManyToManyField(CompanyWebsite, blank=False, default='')


class InvolvedCompany(models.Model):
    id = models.IntegerField(primary_key=True)
    developer = models.BooleanField(null=False, default=False)
    porting = models.BooleanField(null=False, default=False)
    publisher = models.BooleanField(null=False, default=False)
    supporting = models.BooleanField(null=False, default=False)
    game = models.ForeignKey(Games, on_delete=models.CASCADE, null=False, default='')
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=False, default='')