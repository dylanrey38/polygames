from rest_framework import serializers

from games.models import *


class GamesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Games
        fields = ('id',
                  'name',
                  'cover',
                  'slug',
                  'status',
                  'storyline',
                  'summary',
                  'aggregated_rating',
                  'aggregated_rating_count',
                  'multiplayer',
                  'franchise',
                  'category',
                  'age_rating',
                  'game_engine',
                  'game_mode',
                  'genre',
                  'keyword',
                  'platform',
                  'release_date',
                  'video',
                  'websites',
                  'themes',
                  'cover_2',
                  'rating')


class FranchiseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Franchise
        fields = ('id',
                  'name')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id',
                  'name')


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id',
                  'name')


class GameModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameMode
        fields = ('id',
                  'name')


class GameEngineSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameEngine
        fields = ('id',
                  'name',
                  'description')


class GameVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameVideo
        fields = ('id',
                  'name',
                  'video_id')


class PlatformSerializer(serializers.ModelSerializer):
    class Meta:
        model = Platform
        fields = ('id',
                  'name')


class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        fields = ('id',
                  'name')


class ReleaseDateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReleaseDate
        fields = ('id',
                  'date',
                  'region')


class AlternativeNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlternativeName
        fields = ('id',
                  'name',
                  'game')


class AgeRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = AgeRating
        fields = ('id',
                  'category',
                  'rating',
                  'synopsis')


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id',
                  'name',
                  'description')


class InvolvedCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = InvolvedCompany
        fields = ('id',
                  'developer',
                  'porting',
                  'publisher',
                  'supporting',
                  'game',
                  'company')


class WebsiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Website
        fields = ('id',
                  'trusted',
                  'url',
                  'category',
                  'game',
                  'company')


class MultiplayerModeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultiplayerMode
        fields = ('id',
                  'campaign_coop',
                  'drop_in',
                  'lan_coop',
                  'offline_coop',
                  'offline_coop_max',
                  'offline_max',
                  'online_coop',
                  'online_coop_max',
                  'online_max',
                  'split_screen',
                  'split_screen_online')
