from django.conf.urls import url
from games import views

urlpatterns = [
    url(r'^games$', views.games_list),
    url(r'^game_info/(?P<idgame>\d+)$', views.get_game_infos),
    url(r'^bangames', views.writeUnbanGames),
    url(r'^best_new_games/(?P<idUser>\d+)$', views.get_best_new_games),
    url(r'^best_rated_games/(?P<idUser>\d+)$', views.get_best_rated_games),
    url(r'^best_preference_games/(?P<idUser>\d+)$', views.get_best_preference_games),
    url(r'^research', views.research_game),
    url(r'^other_liked_games/(?P<idUser>\d+)$', views.get_other_liked_games),
    url(r'^userlike/(?P<idUser>\d+)/(?P<idGame>\d+)$', views.userLike),
    url(r'^useraddlist/(?P<idUser>\d+)/(?P<idGame>\d+)$', views.addOrRemoveListGame),
    url(r'^gameLikedByUser/(?P<idUser>\d+)$', views.getLikedGame),
    url(r'^gameAddToList/(?P<idUser>\d+)$', views.getListGame),
    url('getAllCats', views.getAllCats)
]
