from django.db import models

from games.models import Games
from games.models import Genre


class Users(models.Model):
    name = models.CharField(max_length=70, blank=False, default='')
    firstname = models.CharField(max_length=70, blank=False, default='')
    mail = models.CharField(max_length=70, blank=False, default='')
    username = models.CharField(max_length=70, blank=False, default='')
    password = models.CharField(max_length=200, blank=False, default='')
    pictureurl = models.CharField(max_length=200, blank=True, default='images/empty_pp/blank-profile-picture.png')


class UsersCluster(models.Model):
    idCluster = models.BigIntegerField(blank=False, default='')
    user = models.ForeignKey(Users, on_delete=models.CASCADE, blank=False, default='')


class UsersPreference(models.Model):
    user = models.ForeignKey(Users, to_field='id', on_delete=models.CASCADE, blank=False, default='')
    genre = models.ForeignKey(Genre, to_field='id', on_delete=models.CASCADE, blank=False, default='')
    value = models.IntegerField(blank=False, default=50)


class UsersReco(models.Model):
    user = models.ForeignKey(Users, to_field='id', on_delete=models.CASCADE, blank=False, default='')
    gameListBest = models.TextField(blank=False, default='1942,1020,19560,72,25076,472,1009,7346,7331,732,149292,26192,71,233,139399')
    gameListFresh = models.TextField(blank=False, default='127358,26775,141503,139399,139090,103281,24417,149292,71595,15698,14741,116200,87067,55029,144765')
    gameListForYou = models.TextField(blank=False, default='null')
    gameListOthersLike = models.TextField(blank=False, default='null')
    gameListLike = models.TextField(blank=False, default='null')
    gameListMyList = models.TextField(blank=False, default='null')
    interactionsNb = models.IntegerField(blank=False, default=0)

class Document(models.Model):
    docfile = models.FileField(upload_to='documents/%Y/%m/%d')
