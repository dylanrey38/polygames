from django.conf.urls import url
from django.urls import include

from Back import settings
from users import views
from django.conf import settings
from django.conf.urls.static import static

# Inlcude the schema view in our urls.
urlpatterns = [
    url(r'users$', views.users_list, name='User list'),
    url(r'connect', views.user_connect),
    url(r'getprofileinfo/(?P<user_id>\d+)$', views.user_get_profile_info),
    url(r'register', views.user_register),
    url(r'userpp/(?P<user_id>\d+)$', views.user_profile_picture),
    url(r'user_username', views.user_username),
    url(r'picture_upload', views.picture_upload),
    url(r'change_password', views.user_change_password),
    url('test', views.initusers),
    url('doUserLikeGame/(?P<idUser>\d+)/(?P<idGame>\d+)$', views.doUserHaveLikeGame),
    url('doUserAddGame/(?P<idUser>\d+)/(?P<idGame>\d+)$', views.doUserHaveAddGame),
    url('firstUserPreference/(?P<idUser>\d+)', views.first_user_preference)
]
