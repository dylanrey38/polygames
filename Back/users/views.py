from os import path
from django.shortcuts import render, redirect

import json
import numpy
import time
from json import JSONEncoder

from django.http.response import JsonResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from rest_framework.parsers import JSONParser
from rest_framework import status

from argon2 import PasswordHasher

from games.models import Games, Genre
from games.models import Platform
from games.serializers import GamesSerializer
from games.views import update_recos, update_otherslike
from users import calculCluster
from rest_framework.decorators import api_view
from django.core.cache import cache
from users.forms import DocumentForm
from users.models import Users, UsersPreference, UsersReco, Document, UsersCluster

from users.serializers import UsersSerializer
from django.template import RequestContext
from django.http import HttpResponseRedirect

from django.views.decorators.csrf import csrf_exempt


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)


@api_view(['GET', 'POST', 'DELETE'])
def users_list(request):
    if request.method == 'GET':
        users = Users.objects.all()
        title = request.GET.get('title', None)
        if title is not None:
            users = users.filter(title__icontains=title)

        users_serializer = UsersSerializer(users, many=True)
        return JsonResponse(users_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        users_data = JSONParser().parse(request)
        users_serializer = UsersSerializer(data=users_data)
        if users_serializer.is_valid():
            users_serializer.save()
            return JsonResponse(users_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(users_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Users.objects.all().delete()
        return JsonResponse({'message': '{} users were deleted successfully!'.format(count[0])},
                            status=status.HTTP_204_NO_CONTENT)


def user_by_id(request, iduser):
    if request.method == 'GET':
        user = Users.objects.filter(id=iduser)
        serializer = UsersSerializer(user)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def user_connect(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        user = Users.objects.get(username=data['username'])
        ph = PasswordHasher()
        if ph.verify(user.password, data['password']):
            return JsonResponse({'id': user.id}, safe=False)
    else:
        return JsonResponse({'message': 'mauvaise requete'}, status=status.HTTP_204_NO_CONTENT)


def user_test(request):
    user = Users.objects.get(id=2)
    userReco = UsersReco(user=user)
    userReco.save(force_insert=True)


@csrf_exempt
def user_register(request):
    if request.method == 'POST':
        ph = PasswordHasher()
        data = JSONParser().parse(request)
        users = Users.objects.all()
        users_serializer = UsersSerializer(users, many=True)
        for i in range(Users.objects.all().count()):
            if users_serializer.data[i]['username'] == data['username']:
                return JsonResponse({'id': -2})

        if len(data['password']) < 6:
            return JsonResponse({'id': -3})

        if data['name'] != "" and data['firstname'] != "" and data['mail'] != "" and data['username'] != "" and data[
            'password'] != "":
            user = Users(name=data['name'], firstname=data['firstname'], mail=data['mail'], username=data['username'],
                         password=ph.hash(data['password']))
            user.save(force_insert=True)
            genres = Genre.objects.all().order_by('id')
            for genre in genres:
                userPref = UsersPreference(user=user, genre=genre)
                userPref.save()
            userReco = UsersReco(user=user)
            userReco.save()

            return JsonResponse({'id': user.id}, status=status.HTTP_201_CREATED)
        else:
            return JsonResponse({'id': -1})
    else:
        return JsonResponse({'message': 'mauvaise requete'}, status=status.HTTP_204_NO_CONTENT)

@csrf_exempt
def first_user_preference(request, idUser):
    data = JSONParser().parse(request)
    genres = data["genres"].split(",")
    genres = [int(numeric_string) for numeric_string in genres]
    for genre in genres:
        userPrefs = UsersPreference.objects.filter(genre_id=genre)
        for userPref in userPrefs:
            if userPref.user.id == int(idUser):
                userPref.value = 70
                userPref.save()
    recalcul_cluster()
    update_otherslike(idUser)
    update_recos(idUser)
    return JsonResponse("end", safe=False)

@csrf_exempt
def user_profile_picture(request, user_id):
    if request.method == 'GET':
        user = Users.objects.get(id=user_id)
        serializer = UsersSerializer(user)
        return JsonResponse(serializer.data['pictureurl'], safe=False)

    if request.method == 'POST':
        user = Users.objects.get(id=user_id)
        data = JSONParser().parse(request)
        serializer = UsersSerializer(user)
        if serializer.is_valid():
            serializer.data['pictureurl'] = data['newpp']
            serializer.save()
            return JsonResponse({'statut': 1})
        else:
            return JsonResponse({'statut': 0})


def user_get_profile_info(request, user_id):
    if request.method == 'GET':
        user = Users.objects.get(id=user_id)
        serializer = UsersSerializer(user)
        return JsonResponse(serializer.data, safe=False)


@csrf_exempt
def user_username(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        users = Users.objects.all()
        users_serializer = UsersSerializer(users, many=True)
        for i in range(Users.objects.all().count()):
            if users_serializer.data[i]['username'] == data['username']:
                return JsonResponse({'statut': 0})

        user = Users.objects.get(id=data['userid'])
        user.username = data['username']
        user.save()
        return JsonResponse({'statut': 1})


# def user_get_recogame_top():
# def user_get_recogame_latest():
# def user_get_recogame_category():
# def user_get_recogame_bestmarked():

@csrf_exempt
def picture_upload(request):
    print(f"Great! You're using Python 3.6+. If you fail here, use the right version.")
    message = 'Upload as many files as you want!'
    # Handle file upload
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            id_user = form.data['userID']
            newdoc = Document(docfile=request.FILES['docfile'])
            newdoc.author = request.user
            newdoc.save()
            user = Users.objects.get(id=id_user)
            user.pictureurl = newdoc.docfile.name
            user.save()
            # Redirect to the document list after POST
            return JsonResponse({'statut': 1})
        else:
            message = 'The form is not valid. Fix the following error:'
            return JsonResponse({'statut': 1})

@csrf_exempt
def user_change_password(request):
    if request.method == 'POST':
        ph = PasswordHasher()
        data = JSONParser().parse(request)
        user = Users.objects.get(id=data['id'])
        users_serializer = UsersSerializer(user, many=True)

        if ph.verify(user.password, data['nowPassword']):
            if data['password'] == data['confirmPassword']:
                new_password = ph.hash(data['password'])
                user.password = new_password
                user.save()

        return JsonResponse({'id': user.id}, status=status.HTTP_201_CREATED)
    else:
        return JsonResponse({'id': -1})


def recalcul_cluster():
    users = Users.objects.all().order_by('id')
    userMeta = []
    dataset = []
    for user in users:
        userDatas = UsersPreference.objects.filter(user=user).order_by('genre')
        for userData in userDatas:
            userMeta.append(userData.value)
        dataset.append(userMeta)
        userMeta = []
    nbUsers = Users.objects.count()
    clusters = calculCluster(dataset, int(nbUsers / 5))
    usersClusters = UsersCluster.objects.all()
    usersClusters.delete()
    for i in range(nbUsers):
        userCluster = UsersCluster(user=users[i], idCluster=clusters[i])
        userCluster.save()
        print(str(users[i].username) + " || Cluster n°" + str(clusters[i]))


def reset_all():
    UsersPreference.objects.all().delete()
    UsersReco.objects.all().delete()
    UsersCluster.objects.all().delete()
    Users.objects.all().delete()


def initusers(request):
    reset_all()
    ph = PasswordHasher()
    genres = Genre.objects.all().order_by('id')
    firstUser = 0
    for i in range(20):
        name = "name" + str(i + 1)
        firstname = "firstname" + str(i + 1)
        username = "user" + str(i + 1)
        mail = "mail" + str(i + 1) + "@gmail.com"
        password = "password" + str(i + 1)
        user = Users(name=name, firstname=firstname, username=username, mail=mail, password=ph.hash(password))
        user.save()
        if i == 0:
            firstUser = user.id
        for genre in genres:
            pref = UsersPreference(user=user, genre=genre, value=50)
            pref.save()
        userReco = UsersReco(user=user)
        userReco.save()
        print("Utilisateur numéro " + str(i + 1) + " créé !")
    print("Calcul des recommandations pour les utilisateurs...")
    update_recos(firstUser)
    copy_recos(firstUser)
    recalcul_cluster()
    return JsonResponse("Data succesfully init !", safe=False)


def copy_recos(idUser):
    userRecoToCopy = UsersReco.objects.get(user_id=idUser)
    users = Users.objects.all()
    gameListBest = userRecoToCopy.gameListBest
    gameListNew = userRecoToCopy.gameListFresh
    gameListPref = userRecoToCopy.gameListForYou
    for user in users:
        if user.id != int(idUser):
            reco = UsersReco.objects.get(user_id=user.id)
            reco.gameListForYou = gameListPref
            reco.gameListFresh = gameListNew
            reco.gameListBest = gameListBest
            reco.save()


def doUserHaveLikeGame(request, idUser, idGame):
    userReco = UsersReco.objects.get(user_id=idUser)
    content = userReco.gameListLike
    if content != "null":
        gameLiked = userReco.gameListLike.split(",")
        gameLiked = [int(numeric_string) for numeric_string in gameLiked]
        result = int(idGame) in gameLiked
    else:
        result = False
    return JsonResponse(result, safe=False)


def doUserHaveAddGame(request, idUser, idGame):
    userReco = UsersReco.objects.get(user_id=idUser)
    content = userReco.gameListMyList
    if content != "null":
        gameLiked = userReco.gameListMyList.split(",")
        gameLiked = [int(numeric_string) for numeric_string in gameLiked]
        result = int(idGame) in gameLiked
    else:
        result = False
    return JsonResponse(result, safe=False)