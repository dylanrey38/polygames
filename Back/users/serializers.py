from rest_framework import serializers
from users.models import *


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('id',
                  'name',
                  'firstname',
                  'mail',
                  'username',
                  'password',
                  'pictureurl')


class UsersClusterSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersCluster
        fields = ('id',
                  'idCluster',
                  'user')


class UsersPreferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsersPreference
        fields = ('id',
                  'user',
                  'genre',
                  'value'
                  )
