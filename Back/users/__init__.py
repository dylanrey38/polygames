from sklearn.cluster import KMeans


def calculCluster(x, nbClusters):
    model = KMeans(n_clusters=nbClusters)
    model.fit(x)
    return model.predict(x)
