import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans

dataset = [(1, 0.8, 0.3, 1, 0.1), (1, 0.8, 0.4, 1, 0.1), (0.1, 0.2, 0.5, 0.1, 1), (1, 0.8, 0.5, 1, 0.1), (1, 0.8, 0.2, 1, 0.1)]

model = KMeans(n_clusters=4)
model.fit(dataset)
print(model.predict(dataset))