import { Component, OnInit } from '@angular/core';
import {TutorialService} from "../../services/tutorial.service";

@Component({
  selector: 'app-tutorials-list',
  templateUrl: './tutorials-list.component.html',
  styleUrls: ['./tutorials-list.component.css']
})
export class TutorialsListComponent implements OnInit {

  constructor(private tutorialService: TutorialService) { }

  tutorials: any;

  ngOnInit(): void {
    this.retrieveData();
  }

  retrieveData() {
    this.tutorialService.getAll()
      .subscribe(
        data => {
          this.tutorials = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
}
